/* 
 * File:   Grass.hpp
 * Author: Fredrik
 *
 * Created on den 19 december 2014, 02:17
 */

#ifndef GRASS_HPP
#define	GRASS_HPP

#include <SFML/Graphics.hpp>

class Grass : public sf::Drawable, public sf::Transformable
{
public:
    Grass();
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
    sf::Texture _texture;
    sf::Sprite  _sprite;
};

#endif	/* GRASS_HPP */

