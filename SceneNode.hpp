/* 
 * File:   SceneNode.hpp
 * Author: Fredrik
 *
 */

#ifndef SCENENODE_HPP
#define	SCENENODE_HPP

#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>
#include <algorithm>
#include <cassert>

class SceneNode : public sf::Drawable, public sf::Transformable
{
public:
    typedef std::unique_ptr<SceneNode> Ptr;
public:
                    SceneNode();
    void            update(const sf::Time& dt);
    void            attachChild(Ptr child);
    SceneNode::Ptr  detachChild(const SceneNode& node);
private:
    virtual void    updateCurrent(const sf::Time& dt);
    void            updateChildren(const sf::Time& dt);
    void            draw(sf::RenderTarget& target, sf::RenderStates states) const;
    virtual void    drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const;
    void            drawChildren(sf::RenderTarget& target, sf::RenderStates states) const;

protected:
    std::vector<Ptr> _children;
    SceneNode*       _parent;
};

#endif	/* SCENENODE_HPP */

