
#include "Animation.hpp"

Animation::Animation()
: p_timeFrame(sf::Time::Zero)
, p_source({0, 0})
{
    
}

void Animation::updateAnimationCycle(const sf::Time& dt)
{
    // Does nothing by default.
}

void Animation::nextFrame()
{
    // Does nothing by default
}