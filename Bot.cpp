/* 
 * File:   Bot.cpp
 * Author: Fredrik
 * 
 * Created on den 8 februari 2015, 16:32
 */

#include "Bot.hpp"

Bot::Bot(const Character::Type& type)
: m_character(type)
, Collisionable({0, 0, 12, 22})
, m_currentState(Idle)
{
    setOrigin(12 / 2.f, 22 / 2.f);
}

void Bot::interact()
{
    std::cout << "Bot::interact()\n";
}

void Bot::setState(const State& state)
{
    m_currentState = state;
}

void Bot::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(m_character, states);
    
}

void Bot::update(const sf::Time& dt)
{
    m_character.updateAnimationCycle(dt, getVel());
    move(dt.asSeconds() * getVel());
}