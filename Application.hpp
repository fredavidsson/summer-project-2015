/* 
 * File:   Application.hpp
 * Author: Fredrik
 *
 */

#ifndef APPLICATION_HPP
#define	APPLICATION_HPP

#include <SFML/Graphics.hpp>
#include <iostream>
#include "World.hpp"
#include "Menu.hpp"


class Application 
{
public:
    
                    Application();
    void            run();
private:
    
    void    handleEvents();
    void    update(const sf::Time& deltaTime);
    void    render();
    
private:
    
    sf::RenderWindow  _window;
    
    // The actual world
    World             _world;
    
    // The menu
    Menu              _menu;


};

#endif	/* APPLICATION_HPP */

