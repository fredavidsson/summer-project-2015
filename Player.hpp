
#ifndef PLAYER_HPP
#define	PLAYER_HPP

#include <SFML/Graphics.hpp>

#include "Animation.hpp"
#include "Collisionable.hpp"
#include "Character.hpp"

class Player : public Collisionable, public sf::Drawable
{
public:
    Player();
    
    void update(const sf::Time& dt);
    void handleEvents(const sf::Event& event);
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    void handleMovement(const sf::Event::KeyEvent& key, bool isPressed);
    void handleMovement();

private:
    Character           m_character;
    
    // checking movements
    bool Up = false, Down = false, Right = false, Left = false;
};

#endif	/* PLAYER_HPP */

