/* 
 * File:   Button.hpp
 * Author: Fredrik
 *
 */

#ifndef BUTTON_HPP
#define	BUTTON_HPP

#include <SFML/Graphics.hpp>
#include <vector>

class Button : public sf::Drawable, public sf::Transformable
{
public:
    Button(sf::RenderWindow& window);
    void addButton(const sf::Vector2f& position, const sf::Vector2f& size);
    void update(const sf::Time& dt);
    void selectNextButton();
    void selectPreviousButton();
    void selectButton();
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    void toggleColor(sf::RectangleShape& rectangle);

private:
    sf::RenderWindow& _window;
    std::vector<sf::RectangleShape> _buttons;
    // The selected button. An integer for now
    unsigned int _selected;
};

#endif	/* BUTTON_HPP */

