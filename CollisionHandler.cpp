/* 
 * File:   CollisionHandler.cpp
 * Author: Pankie
 * 
 */

#include "CollisionHandler.hpp"

// Prevents player go through a building
void CollisionHandler::handleObstacle(Collisionable& player, Collisionable& building)
{
    // affected area. This will be constructed when player intersects the building
    sf::FloatRect afArea;
    
    // Player's bounds
    sf::FloatRect pBounds = player.getGlobalBounds();
    
    // if the player's bounds intersects with the building, we construct the
    // affected area, the afArea.
    if(pBounds.intersects(building.getGlobalBounds(), afArea))
    {
        // for debugging. This changes the color of the building when player
        // is intersecting with the building.
        // building.changeColor(sf::Color(50, 140, 255));
        
        // verify if we need to apply collision to the vertical or horizontal axis
        // this is for the top and bottom.
        if(afArea.width > afArea.height)
        {
            
            // Top of the character
            if(afArea.contains({ 
                afArea.left, pBounds.top}))
            {
                // move the player downwards.
                player.setPosition({
                    player.getPosition().x,
                    player.getPosition().y + afArea.height
                });
            }
            // Bottom of the character
            else if(afArea.contains({
                afArea.left + 1.f, pBounds.top + pBounds.height - 1.f}))
            {
                // move the player upwards.
                player.setPosition({
                    player.getPosition().x, 
                    player.getPosition().y - afArea.height
                });
            }
        }
        
        // left and the right side of the object
        else if(afArea.width < afArea.height)
        {
            // move the player to the left.
            if(afArea.contains({
                pBounds.left + pBounds.width - 1.f,
                afArea.top + 1.f}))
            {
                player.setPosition({
                    player.getPosition().x - afArea.width,
                    player.getPosition().y});
            }
            
            // Left side of the character
            else if(afArea.contains(pBounds.left, afArea.top))
            {
                // move the player to the right.
                player.setPosition({
                    player.getPosition().x + afArea.width,
                    player.getPosition().y});
            }
        }
    }
}

bool CollisionHandler::collision(Collisionable& object, Collisionable& object_2)
{
    return (object.collision(object_2));
}