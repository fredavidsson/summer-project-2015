# About

Just some RPG game I was doing during summer mainly using the media library SFML.

![Example](https://gitlab.com/fredavidsson/summer-project-2015/raw/master/image10.png)

# Current missing

Need a makefile.

In order to compile on Ubuntu one needs to install the SFML packages and just
run the command:

```
g++ *.cpp -std=c++11 -L ./lib/ -I ./ -I ./include/ -l sfml-graphics -lsfml-window -lsfml-system -o ./build/linux/main.o
```

then run the main.o file in the terminal. 
