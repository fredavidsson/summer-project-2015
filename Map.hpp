/*
 * File:   Map.hpp
 * Author: Fredrik
 *
 */

#ifndef MAP_HPP
#define	MAP_HPP

#include <SFML/Graphics.hpp>
#include "ColRectangle.hpp"
#include "Player.hpp"
#include "MapContainer.hpp"
#include "Json.hpp"
#include <sstream>

// drawable text
struct debugText : public sf::Drawable, sf::Transformable
{
    sf::Text     text;
    sf::Vector2f offset;

    debugText(sf::Font& font)
    : offset(sf::Vector2f(0, 0))
    {
        font.loadFromFile("./Inconsolata.otf");
        text.setFont(font);
        text.setCharacterSize(8);
    }

    virtual void draw(sf::RenderTarget& t, sf::RenderStates st) const
    {
        st.transform *= getTransform();
        t.draw(text, st);
    }

    void setText(const std::string& str)
    {
        text.setString(str);
    }

    void update(const sf::Vector2f& position)
    {
        text.setPosition(position.x + offset.x, position.y + offset.y);
    }

};

// Drawable maps
class Map : public sf::Drawable, public sf::Transformable
{
public:

    Map(Player& player);

    // set spawn position
    void setSpawnPosition(const sf::Vector2f& pos);

    void update(const sf::Time& dt);
    void handleEvents(const sf::Event& event);

private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    // initialize all maps
    void         init();

    // saves all maps
    // contains ugly code.
    void         saveWorlds();

    // loads all maps
    void         loadWorlds();

    // toggles the player's position
    void         debug_display();

public:

    // spawn position for player.
    sf::Vector2f            _spawnPosition;

    // Used to move player to a new position
    Player&                 _player;

    MapContainer            _mapcontainer;

    Json                    _json;

    // for toggling debugging.
    bool                    _debug = false;

    // displays player's position
    sf::Font                _debugFont;
    debugText               _playerPosition_text;
    debugText               _currentMap_text;

};

#endif	/* MAP_HPP */
