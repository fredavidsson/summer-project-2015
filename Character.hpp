/* 
 * File:   Character.hpp
 * Author: Fredrik
 *
 */

#ifndef CHARACTER_HPP
#define	CHARACTER_HPP

#include "Animation.hpp"
#include <SFML/Graphics.hpp>

class Character : public Animation, public sf::Drawable
{
public:
    enum Type 
    {
        Villager,
        Salesman
    };
    Character(const Type& type);
public:
    void         updateAnimationCycle(const sf::Time& dt, const sf::Vector2f& velocity);
    virtual void nextFrame();
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    const std::string getFilePath(const Type& type);
    
private:
    sf::Texture m_texture;
    
    // For the character's movement.
    enum Direction { Down, Left, Right, Up };
};

#endif	/* CHARACTER_HPP */

