/* 
 * File:   SceneNode.cpp
 * Author: Fredrik
 * 
 * Created on den 19 december 2014, 04:38
 */

#include "SceneNode.hpp"

SceneNode::SceneNode()
: _children()
, _parent(nullptr)
{
}

void SceneNode::attachChild(Ptr child)
{
    child->_parent = this;
    _children.push_back(std::move(child));
}

SceneNode::Ptr SceneNode::detachChild(const SceneNode& node)
{
    auto found = std::find_if(_children.begin(), _children.end(), 
        [&] (Ptr& p) { return p.get() == &node; });
        
    assert(found != _children.end());
    
    Ptr result = std::move(*found);
    result->_parent = nullptr;
    _children.erase(found);
    return result;
}

void SceneNode::update(const sf::Time& dt)
{
    updateCurrent(dt);
    updateChildren(dt);
}

void SceneNode::updateCurrent(const sf::Time& dt)
{
    // Nothing by default
}

void SceneNode::updateChildren(const sf::Time& dt)
{
    for(Ptr& child : _children)
    {
        child->update(dt);
    }
}

void SceneNode::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    drawCurrent(target, states);
    drawChildren(target, states);
}

void SceneNode::drawChildren(sf::RenderTarget& target, sf::RenderStates states) const
{
    for(const Ptr& child : _children)
    {
        child->draw(target, states);
    }
}

void SceneNode::drawCurrent(sf::RenderTarget& target, sf::RenderStates states) const
{
    // does nothing by default
}