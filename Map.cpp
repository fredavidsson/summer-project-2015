/*
 * File:   Map.cpp
 * Author: Fredrik
 *
 */

#include "Map.hpp"
#include "World.hpp"

Map::Map(Player& player)
: _player(player)
, _mapcontainer()
, _playerPosition_text(_debugFont)
, _currentMap_text(_debugFont)
{
    init();
}

void Map::init()
{
    assert(_debugFont.loadFromFile("./Inconsolata.otf"));
    _currentMap_text.offset         = sf::Vector2f(25, 108);
    _playerPosition_text.offset     = sf::Vector2f(25, 120);

    loadWorlds();
}

void Map::loadWorlds()
{

    // Loads the file to a json document object
    const Json::Document& document = _json.load("world.json");
    _mapcontainer.addMaps(document);

    // we load the world map as a starting area.
    _mapcontainer.loadMap(MapContainer::World);
}

void Map::saveWorlds()
{
    const rapidjson::Document& document = _mapcontainer.getJsonMaps();
    _json.save("world.json", document);
}


void Map::setSpawnPosition(const sf::Vector2f& pos)
{
    _spawnPosition = pos;
}

void Map::handleEvents(const sf::Event& event)
{
    switch(event.type)
    {
    case sf::Event::KeyReleased:
        // if Y was pressed, reload the maps.
        if(event.key.code == sf::Keyboard::F1)
        {
            std::cout << "attempt to load maps\n";
            const rapidjson::Document& doc = _json.load("world.json");
            _mapcontainer.reloadMaps(doc);
        }
        // if F2 was pressed, save the maps
        else if(event.key.code == sf::Keyboard::F2)
        {
            std::cout << "attempt to save maps\n";
            saveWorlds();
        }
        // if F3, toggle debug information
        else if(event.key.code == sf::Keyboard::F3)
        {
            std::cout << "toggling debugmode\n";
            debug_display();

        }
        break;
    default:
        break;
    }
}

void Map::debug_display()
{
    _debug ? _debug = false : _debug = true;
}

void Map::update(const sf::Time& dt)
{
    _mapcontainer.update(dt, _player);
    if(_debug)
    {
        std::ostringstream oss;
        oss << "player_position: "
            << _player.getPosition().x << ", " << _player.getPosition().y;

        _playerPosition_text.setText(oss.str());
        oss.str("");

        _playerPosition_text.update(_player.getPosition());

        oss << "current_map: " << static_cast<size_t>(_mapcontainer.getCurrentMapKey());
        _currentMap_text.setText(oss.str());
        _currentMap_text.update(_player.getPosition());
    }
}

void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(_mapcontainer, states);
    if(_debug)
    {
        // draw the debug text
        target.draw(_playerPosition_text, states);
        target.draw(_currentMap_text, states);
    }
}
