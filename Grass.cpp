/* 
 * File:   Grass.cpp
 * Author: Fredrik
 * 
 * Created on den 19 december 2014, 02:17
 */

#include "Grass.hpp"

Grass::Grass()
{
    _texture.loadFromFile("./Grass_normal.jpg");
    _texture.setRepeated(true);
    _texture.setSmooth(true);
    _sprite.setTexture(_texture);
    _sprite.setTextureRect(sf::IntRect(0, 0, 1280, 720));
    
}

void Grass::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(_sprite, states);
}