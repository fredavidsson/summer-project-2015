/* 
 * File:   Character.cpp
 * Author: Fredrik
 * 
 */

#include "Character.hpp"
#include "Application.hpp"

Character::Character(const Type& type)
{
    if(!m_texture.loadFromFile(getFilePath(type)))
    {
        std::cerr << "Couldn't load file: " << getFilePath(type) << '\n';
    }
    p_sprite.setTexture(m_texture);
    p_sprite.setTextureRect(
        sf::IntRect(p_source.x * 24, p_source.y * 24, 24, 24));
}

const std::string Character::getFilePath(const Type& type)
{
    switch(type)
    {
    case Type::Villager:
        return std::string("./character_3.png");
        break;
    default:
        return std::string("./character_3.png");
        break;
    }
}


void Character::updateAnimationCycle(const sf::Time& dt, const sf::Vector2f& velocity)
{
    // Keeps the animation cycle from not going too fast.
    p_timeFrame += dt;
    
        // If the character is moving upwards
        if(velocity.y < 0)
        {
            // Change the vector y to 4
            p_source.y = Direction::Up;
            // If enough time has elapsed, go for the next frame
            if(p_timeFrame.asSeconds() > 0.25)
            {
                nextFrame();         
            }
        }
        else if(velocity.y > 0)
        {
            p_source.y = Direction::Down;
            if(p_timeFrame.asSeconds() > 0.25)
            {
                nextFrame();
            }
        }
        else if(velocity.x > 0)
        {
            p_source.y = Direction::Right;
            if(p_timeFrame.asSeconds() > 0.25)
            {
                nextFrame();
            }
        }  
        else if(velocity.x < 0)
        {
            p_source.y = Direction::Left;
            if(p_timeFrame.asSeconds() > 0.25)
            {
                nextFrame();
            }
        }
        else
        {
            p_source.x = 0;
        }

        // Update sprite
        p_sprite.setTextureRect(sf::IntRect(p_source.x * 24, p_source.y * 24, 24, 24));
        // sf::IntRect(p_source.x * 32, p_source.y * 48, 32, 48));
}

void Character::nextFrame()
{
    // Make sure the current frame doesn't get out of bounds on the texture.
    if(p_source.x >= 3)
        p_source.x = 0;
    else
        p_source.x++;

    // reset the timer.
    p_timeFrame = sf::Time::Zero;
}

void Character::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(p_sprite, states);
}
