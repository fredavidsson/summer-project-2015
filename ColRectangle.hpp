
#ifndef BUILDING_HPP
#define	BUILDING_HPP

#include <SFML/Graphics.hpp>
#include "Player.hpp"
#include "Collisionable.hpp"

// A collisionable rectangle.
class ColRectangle : public Collisionable, public sf::Drawable
{
public:
    ColRectangle();
    ColRectangle(sf::Vector2f size);
    
    // Edit the size of the shape and it's bounds.
    // This also centers the origin
    void setSize(const sf::Vector2f& size);
    
    // returns the size of the shape
    const sf::Vector2f& getSize();
    
    // changes color of the shape.
    // doesn't change if the colour already has been changed.
    void changeColor(const sf::Color& color);
    
    // returns color of the shape
    const sf::Color& getFillColor();
    
private:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
    
    // Sets the origin to the center of the shape
    void resetOrigin();
    
private:        
    sf::RectangleShape shape;
};

#endif	/* BUILDING_HPP */

