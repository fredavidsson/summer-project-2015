/* 
 * File:   MapContainer.cpp
 * Author: Fredrik
 * 
 */

#include <map>

#include "MapContainer.hpp"
#include "World.hpp"

MapContainer::MapContainer()
: m_spawnPosition()
{
}

void MapContainer::reloadMaps(const rapidjson::Document& document)
{
    std::cout << "reloading maps\n";
    const rapidjson::Value& a = document["maps"];
    // store the current level key while removing old levels.
    MapContainer::LEVEL oldLevel = m_currentLevel;
    
    // break the program if the document isn't an array.
    assert(a.IsArray());
    
    for(size_t i = 0; i < a.Size(); i++)
    {
        deleteMap(a[i]["name"].GetInt());
        
    }
    addMaps(document);
    
    // load the stored key for the map
    loadMap(oldLevel);
}

void MapContainer::deleteMap(const unsigned int& level)
{
    deleteMap(static_cast<MapContainer::LEVEL>(level));
}

void MapContainer::deleteMap(MapContainer::LEVEL level)
{
    auto found = m_mapHolder.find(level);
    assert(found != m_mapHolder.end());
    m_mapHolder.erase(found);
    
    delete found->second.get();
    
}

void MapContainer::addMaps(const rapidjson::Document& document)
{

    ////////////////////////////////////////////////////////////////////////////
    // Build the worlds! ///////////////////////////////////////////////////////
    const rapidjson::Value& a = document["maps"];
    
    // check if it's an array.
    assert(a.IsArray());
    
    // prints and stores the buildings, floor and spawn positions to the maps
    for(rapidjson::SizeType i = 0; i < a.Size(); i++)
    {   
        // integer is basically the same as an enum in this case.
        insertMap(a[i]["name"].GetInt());
        
        // loads the map and prepare for inserting models
        loadMap(a[i]["name"].GetInt());
        
        addBuilding(a[i]);
        setSpawnPosition(a[i]);
        addFloor(a[i]);
        addBots(a[i]["bots"]);
    }
}

void MapContainer::addBots(const rapidjson::Value& val)
{
    
    assert(val.IsArray());
    
    Json json;
    
    for(rapidjson::SizeType i = 0; i < val.Size(); i++)
    {
        Character::Type  type = static_cast<Character::Type>(val[i]["type"].GetInt());
        Bot::State      state = static_cast<Bot::State>(val[i]["state"].GetInt());
        
        // for some reason the bots didn't render, but as a pointer type they do.
        BotPtr bot(new Bot(type));
        bot->setState(state);
        bot->setPosition(json.getVector(val[i]["bot_position"]));
        addBot(bot);
    }
    
}

void MapContainer::addBot(BotPtr& bot)
{
    getMap().m_bots.push_back(std::move(bot));
    
}

void MapContainer::addFloor(const sf::RectangleShape& rectangle)
{
    getMap().m_floor = rectangle;
}

void MapContainer::Building::addWall(const ColRectangle& wall)
{
    Wall.push_back(wall);
}

void MapContainer::Building::addWall(ColRectangle& wall)
{
    Wall.push_back(std::move(wall));
}

void MapContainer::Building::addDoor(const Door& door)
{
    Doors.push_back(std::move(door));
}

void MapContainer::addBuilding(const Building& building)
{
    getMap().m_buildings.push_back(building);
}

MapContainer& MapContainer::getMap() const
{
    auto found = m_mapHolder.find(m_currentLevel);
    assert(found != m_mapHolder.end());
    
    return *found->second;
}

void MapContainer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    MapContainer& current = getMap();
    
    // draws the floor of the current selected map
    target.draw(current.m_floor, states);
    
    // draw each building
    for(const Building& building : current.m_buildings)
    {
        
        // draw all the walls inside the Building class
        for(const ColRectangle& wall : building.Wall)
        {
            target.draw(wall, states);
        }
        
        // draws the door for each building, if any.
        //target.draw(building.Door, states);
        for(const ColRectangle& door : building.Doors)
        {
            target.draw(door, states);
        }
    }
    
    // draw all bots
    for(const BotPtr& bot : current.m_bots)
    {
        target.draw(*bot, states);
    }
}

void MapContainer::addBuilding(const rapidjson::Value& val)
{
    Json json;
    Building building;
    
    const rapidjson::Value& walls = val["buildings"]["building"]["walls"];
    const rapidjson::Value& doors = val["buildings"]["building"]["doors"];
    
    assert(walls.IsArray());
    
    // add all walls
    for(rapidjson::SizeType i = 0; i < walls.Size(); i++)
    {
        ColRectangle wall;
        
        sf::Vector2f size = json.getVector(walls[i]["wall_size"]);
        wall.setSize(size);

        sf::Color color = json.getColor(walls[i]["wall_color"]);
        wall.changeColor(color);
        
        sf::Vector2f pos = json.getVector(walls[i]["wall_position"]);
        wall.setPosition(pos);
        
        // store wall to the building
        building.addWall(std::move(wall));
    }
    
    // add all doors
    for(rapidjson::SizeType i = 0; i < doors.Size(); i++)
    {
        Door door;
        door.setSize(json.getVector(doors[i]["door_size"]));
        door.setPosition(json.getVector(doors[i]["door_position"]));
        door.changeColor(json.getColor(doors[i]["door_color"]));
        door.setStartPos(json.getVector(doors[i]["start_pos"]));
        door.setKeyMap(static_cast<MapContainer::LEVEL>(doors[i]["key_map"].GetInt()));
        
        building.addDoor(door);
    }
    // add the building to the current build
    addBuilding(building);
}

MapContainer& MapContainer::getMap(const size_t& level)
{
    auto found = m_mapHolder.find(static_cast<LEVEL>(level));
    assert(found != m_mapHolder.end());
    
    return *found->second;
}

const rapidjson::Document MapContainer::getJsonMaps()
{
    rapidjson::Document document;
    document.SetObject();
    rapidjson::Document::AllocatorType& allocator = document.GetAllocator();
    rapidjson::Value    array(rapidjson::kArrayType);
    Json                json;
    
    // stores the default map while cycling through all maps
    // because we're using this variable for now.
    LEVEL lastLevel = m_currentLevel;
    
    for(size_t i = 0; i < LEVEL::Default; i++)
    {
        
        m_currentLevel = static_cast<LEVEL>(i);

        rapidjson::Value jsonMap(rapidjson::kObjectType);
        jsonMap.AddMember("name", i, allocator);
        jsonMap.AddMember(
            "spawn_position",
            json.getVector(getSpawnPosition(), allocator),
            allocator);
        
        jsonMap.AddMember(
            "floor",
            getJsonFloor(allocator),
            allocator);
        
        jsonMap.AddMember(
            "buildings",
            getJsonBuilding(allocator),
            allocator);
        
        array.PushBack(std::move(jsonMap), allocator);
    }
    
    document.AddMember("maps", std::move(array), allocator);
    
    // change back to the current level
    loadMap(lastLevel);
    
    return document;
}

void MapContainer::addFloor(const rapidjson::Value& val)
{
    if(val.HasMember("floor"))
    {
        MapContainer& current = getMap();
        Json json;

        // set the floor size
        sf::Vector2f size = json.getVector(val["floor"]["floor_size"]);
        sf::RectangleShape floor(size);

        // set the color of the floor
        sf::Color color = json.getColor(val["floor"]["floor_color"]);

        floor.setFillColor(color);
        floor.setOrigin({floor.getSize().x / 2.f, floor.getSize().y / 2.f});

        // sets the position of the floor
        sf::Vector2f pos = json.getVector(val["floor"]["floor_position"]);
        floor.setPosition(pos);


        current.m_floor = floor;
    }
}

rapidjson::Value MapContainer::getJsonFloor(rapidjson::Document::AllocatorType& allocator)
{
    MapContainer& current = getMap();
    Json json;
    
    rapidjson::Value floorObj(rapidjson::kObjectType);
    
    floorObj.AddMember(
        "floor_size", 
        json.getVector(current.m_floor.getSize(), allocator), allocator);
    floorObj.AddMember(
        "floor_position", 
        json.getVector(current.m_floor.getPosition(), allocator), allocator);
    floorObj.AddMember(
        "floor_color", 
        json.getColor(current.m_floor.getFillColor(), allocator), allocator);
    
    return floorObj;
}

rapidjson::Value MapContainer::getJsonBuilding(rapidjson::Document::AllocatorType& allocator)
{  
    rapidjson::Value value(rapidjson::kObjectType);
    MapContainer& current = getMap();
    
    // add all walls, doors and positions to the json object
    // from the current Map
    for(Building& building : current.m_buildings)
    {
        rapidjson::Value bValue(rapidjson::kObjectType);
        rapidjson::Value arrWalls(rapidjson::kArrayType);
        rapidjson::Value arrDoors(rapidjson::kArrayType);
        
        for(ColRectangle& wall : building.Wall)
        {
            // store the following wall into a json object
            rapidjson::Value bWall(rapidjson::kObjectType);
            bWall = getJsonWall(allocator, wall);
    
            arrWalls.PushBack(bWall, allocator);
        }
        
        for(Door& door : building.Doors)
        {
            // store the following door into a json object
            rapidjson::Value bDoor(rapidjson::kObjectType);
            
            bDoor = getJsonDoor(allocator, door);
            arrDoors.PushBack(bDoor, allocator);
        }
        
        
        bValue.AddMember("walls", arrWalls, allocator);
        bValue.AddMember("doors", arrDoors, allocator);
        
        value.AddMember("building", bValue, allocator);
    }
    
    return value;
}

rapidjson::Value MapContainer::getJsonWall(
    rapidjson::Document::AllocatorType& allocator, 
    ColRectangle& wall)
{
    rapidjson::Value json(rapidjson::kObjectType);
    Json             jsonB;
    
    rapidjson::Value wall_size      = jsonB.getVector(wall.getSize(), allocator);
    rapidjson::Value wall_position  = jsonB.getVector(wall.getPosition(), allocator);
    rapidjson::Value wall_color     = jsonB.getColor(wall.getFillColor(), allocator);
    
    json.AddMember("wall_size",     std::move(wall_size),       allocator);
    json.AddMember("wall_position", std::move(wall_position),   allocator);
    json.AddMember("wall_color",    std::move(wall_color),      allocator);
 
    return json;
}

rapidjson::Value MapContainer::getJsonDoor(
    rapidjson::Document::AllocatorType& allocator, 
    Door& door)
{
    rapidjson::Value json(rapidjson::kObjectType);
    Json             jsonB;
    
    rapidjson::Value    door_color      = jsonB.getColor(door.getFillColor(), allocator);
    rapidjson::Value    door_position   = jsonB.getVector(door.getPosition(), allocator);
    rapidjson::Value    door_size       = jsonB.getVector(door.getSize(),     allocator);
    rapidjson::Value    start_pos       = jsonB.getVector(door.getStartPos(), allocator);
    MapContainer::LEVEL key_map         = door.getKeyMap();
    
    
    json.AddMember("door_color",    std::move(door_color),      allocator);
    json.AddMember("door_position", std::move(door_position),   allocator);
    json.AddMember("door_size",     std::move(door_size),       allocator);
    json.AddMember("key_map",       std::move(key_map),         allocator);
    json.AddMember("start_pos",     std::move(start_pos),       allocator);
    
    
    return json;
}


void MapContainer::update(const sf::Time& dt, Player& player)
{
    MapContainer& current = getMap();

    // update bots and check for collisions
    for(BotPtr& bot : current.m_bots)
    {
        bot->update(dt);
        World::colHandler.handleObstacle(player, *bot);
    }
    
    // check for collision for each wall inside the Building class
    for(Building& build : current.m_buildings)
    {
        // Check each wall for collisions and handle them. 
        for(ColRectangle& wall : build.Wall)
            World::colHandler.handleObstacle(player, wall);
        
        // check each door for collisions
        for(size_t i = 0; i < build.Doors.size(); i++)
        {
            Door& door = build.Doors[i];
            
            if(World::colHandler.collision(player, door))
            {
                // load the map where the door leads
                loadMap(door.getKeyMap());
                
                // move player to the start position for the level
                player.setPosition(door.getStartPos());
            }
            else
            {
                door.changeColor(sf::Color::Yellow);
            }
        }
        
    }
    
}

void MapContainer::insertMap(const size_t& level)
{
    insertMap(static_cast<MapContainer::LEVEL>(level));
}


void MapContainer::insertMap(MapContainer::LEVEL level)
{
    Ptr pointer(new MapContainer());
    auto inserted = m_mapHolder.insert(std::make_pair(level, std::move(pointer)));
    assert(inserted.second);
}

void MapContainer::loadMap(MapContainer::LEVEL level)
{
    m_currentLevel = level;
}

void MapContainer::loadMap(const size_t& level)
{
    loadMap(static_cast<MapContainer::LEVEL>(level));
}

void MapContainer::setSpawnPosition(const sf::Vector2f& newPosition)
{
    getMap().m_spawnPosition = newPosition;
}

void MapContainer::setSpawnPosition(const rapidjson::Value& obj)
{
    Json json;
    setSpawnPosition(json.getVector(obj["spawn_position"]));
}

const sf::Vector2f& MapContainer::getSpawnPosition()
{
    return getMap().m_spawnPosition;
}

MapContainer::Door::Door()
{
    leadsTo = MapContainer::World;
}

void MapContainer::Door::setKeyMap(MapContainer::LEVEL level)
{
    leadsTo = level;
}

const MapContainer::LEVEL MapContainer::Door::getKeyMap()
{
    return leadsTo;
}

void MapContainer::Door::setStartPos(const sf::Vector2f& start)
{
    startPos = start;
}

const sf::Vector2f& MapContainer::Door::getStartPos()
{
    return startPos;
}