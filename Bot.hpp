/* 
 * File:   Bot.hpp
 * Author: Fredrik
 *
 * Created on den 8 februari 2015, 16:32
 */

#ifndef BOT_HPP
#define	BOT_HPP

#include "Character.hpp"
#include "Collisionable.hpp"
#include <iostream>

class Bot : public Collisionable, public sf::Drawable
{
public:
    enum State
    {
        Idle,
        Dead,
        Angressive,
        Fleeing
    };
    
    // Character types: Salesman, Villager.. 
    Bot(const Character::Type& type);
    
    // Current state for the character.
    void setState(const State& state);
    
    void update(const sf::Time& dt);

    // Interacts with the Bot.
    void interact();
    
private:
        
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

private:
    Character m_character;
    State     m_currentState;
};

#endif	/* BOT_HPP */

