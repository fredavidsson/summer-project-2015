/* 
 * File:   File.hpp
 * Author: Fredrik
 *
 */

#ifndef FILE_HPP
#define	FILE_HPP

#include <fstream>
#include <iostream>
#include <string>

class File 
{
public:
    
    // returns a copy of the file contents for the path given in parameter
    std::string readFile(const std::string& filePath);
    
    // writes a string to a file or creates a new one if file path wasn't found.
    void writeFile(const std::string& filePath, const std::string& text);
    
private:
    
};

#endif	/* FILE_HPP */

