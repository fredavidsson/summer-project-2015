#include "World.hpp"
#include "Application.hpp"

// static
CollisionHandler World::colHandler;

World::World(sf::RenderWindow& window)
: m_window(window)
, m_player() 

// View for the player
, m_playersView()

// Handles collisions and draws the map
, m_map(m_player)
{
    m_player.setPosition(0, 0);
    
    // sets the view to player's current position
    m_playersView.setCenter(m_player.getPosition());
    m_playersView.setSize(400, 300);
    m_window.setView(m_playersView);
    
}

void World::updateViewPosition()
{
    m_playersView.setCenter(m_player.getPosition());
    m_window.setView(m_playersView);
}

void World::handleEvents(const sf::Event& event)
{
    m_player.handleEvents(event);
    
    // handling events for debugging, for now.
    m_map.handleEvents(event);
}

void World::render()
{
    // draw the map
    m_window.draw(m_map);
    
    // draw the player
    m_window.draw(m_player);
}

void World::update(const sf::Time& deltaTime)
{
    m_player.update(deltaTime);
    m_map.update(deltaTime);
    

    updateViewPosition();
}