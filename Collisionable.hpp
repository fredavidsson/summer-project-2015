
#ifndef COLLISIONABLE_HPP
#define	COLLISIONABLE_HPP

#include <iostream>
#include <SFML/Graphics.hpp>

#include "Entity.hpp"

// Makes it able to get detected if collision occurs
class Collisionable : public Entity
{
public:
    Collisionable();
    Collisionable(const sf::Vector2f& size);
    Collisionable(const sf::FloatRect& rect);
    sf::FloatRect getGlobalBounds() const;
    bool collision(Collisionable& col);
    void setBounds(const sf::FloatRect& rect);
    
public:
    sf::FloatRect bounds;
};

#endif	/* COLLISIONABLE_HPP */

