/* 
 * File:   Entity.cpp
 * Author: Fredrik
 * 
 * Created on den 29 oktober 2014, 15:34
 */

#include "Entity.hpp"

void Entity::update(const sf::Time& dt)
{
    move(velocity * dt.asSeconds());
}

const sf::Vector2f Entity::getVel()
{
    return velocity;
}

void Entity::setVel(const sf::Vector2f& v)
{
    velocity = v;
}