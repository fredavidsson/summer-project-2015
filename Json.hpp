/* 
 * File:   Json.hpp
 * Author: Fredrik
 *
 */

#ifndef JSON_HPP
#define	JSON_HPP

#include "File.hpp"
#include "MapContainer.hpp"
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/prettywriter.h>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <cassert>


class Json : public File
{
public:
    typedef rapidjson::Value                    Value;
    typedef rapidjson::Document                 Document;
    typedef rapidjson::Document::AllocatorType  AllocatorType;
    
public:
    // test.
    void save();
    
    // returns a json Value from vector object
    Value getVector(const sf::Vector2f& vector, AllocatorType& all);
    
    // returns a vector from json value
    const sf::Vector2f getVector(const Value& val);
    
    // returns a color object as json Value
    Value getColor(const sf::Color& color, AllocatorType& all);
    
    // returns a sf::color object from json value
    const sf::Color getColor(const Value& val);
    
    // returns the file from given path
    const rapidjson::Document load(const std::string& filePath);
    
    // saves the document
    void save(const std::string& filePath, const Document& document);
    
    // prints the content of the document.
    void print(const Document& doc);
    
    // prints the content of the json Value object
    void print(const Value& val);
    
private:
    
    
};

#endif	/* JSON_HPP */

