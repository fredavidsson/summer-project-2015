
#ifndef ANIMATION_HPP
#define	ANIMATION_HPP

#include <SFML/Graphics.hpp>
#include <string>

class Animation
{
public:
    Animation();
    virtual void updateAnimationCycle(const sf::Time& dt);
    
    // Go to the next frame
    // Does nothing by default.
    virtual void nextFrame();    
protected:
    // Keeps the animation cycle from not going too fast.
    sf::Time     p_timeFrame;
    
    // helps the animation to cycle
    sf::Vector2i p_source;
    
    // Stored 
    sf::Sprite   p_sprite;
    
};

#endif	/* ANIMATION_HPP */

