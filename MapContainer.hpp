/* 
 * File:   MapContainer.hpp
 * Author: Fredrik
 *
 */

#ifndef MAPCONTAINER_HPP
#define	MAPCONTAINER_HPP

#include <SFML/Graphics.hpp>
#include "ColRectangle.hpp"
#include <memory>
#include <vector>
#include <cassert>
#include "rapidjson/document.h"
#include "Json.hpp"
#include "Bot.hpp"

class MapContainer : public sf::Drawable, public sf::Transformable
{
public:
    typedef std::unique_ptr<MapContainer> Ptr;
    typedef std::unique_ptr<Bot>          BotPtr;
    
    enum LEVEL
    {
        World,
        House,
        Default // Doesn't exist.
    };
    
    struct Door : public ColRectangle
    {
        Door();
        
        // Where the level leads to when player collisions with the door
        void                      setKeyMap(MapContainer::LEVEL level);
        
        // Returns a key where this door leads to 
        const MapContainer::LEVEL getKeyMap();
        
        // sets start position from when the player steps out the door
        void                      setStartPos(const sf::Vector2f& start);
        
        // returns position from where the player should be spawned after
        // stepping out the door.
        const sf::Vector2f&       getStartPos();
        
        // when stepping on to this door, it'll lead to the level given here
        MapContainer::LEVEL leadsTo;
        
        // this is the start position after the player stepped out the door.
        sf::Vector2f        startPos;
    };
    
    // Building object
    struct Building
    {
        // adds a wall to the Building object.
        void addWall(const ColRectangle& wall);
        void addWall(ColRectangle& wall);
        
        // adds a door to the Building object.
        void addDoor(const MapContainer::Door& door);
        
        std::vector<ColRectangle>       Wall;
        std::vector<MapContainer::Door> Doors;
    };
    
    
public:
    
    MapContainer();
    
    // loads the maps from json document
    void reloadMaps(const rapidjson::Document& document);
    
    // loads all maps from json document
    void addMaps(const rapidjson::Document& document);
     
    // changes the map
    void loadMap(MapContainer::LEVEL level);
    
    // changes the map
    void loadMap(const size_t& level);
    
    const MapContainer::LEVEL& getCurrentMapKey() { return m_currentLevel; };
     
    // stores everything in a json document and returns it
    const rapidjson::Document getJsonMaps();
    
    // prevents player from going through buildings
    void update(const sf::Time& dt, Player& player);
    
private:
    // draws the current map that has been loaded by MapContainer::loadMap
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    // returns all buildings from current selected object
    // as json object.
    rapidjson::Value getJsonBuilding(rapidjson::Document::AllocatorType& allocator);
    
    // returns the floor from the current building
    rapidjson::Value getJsonFloor(rapidjson::Document::AllocatorType& allocator);
    
    // returns given door as json object
    rapidjson::Value getJsonDoor(rapidjson::Document::AllocatorType& allocator, Door& door);
    
    // return given wall as json object
    rapidjson::Value getJsonWall(rapidjson::Document::AllocatorType& allocator, ColRectangle& wall);

    // adds building to a current level.
    void addBuilding(const Building& building);
    
    // adds the floor to the current level.
    void addFloor(const sf::RectangleShape& rectangle);
    
    // adds the floor to the current level.
    void addFloor(const rapidjson::Value& val);
    
    // adds a building to the current level.
    void addBuilding(const rapidjson::Value& val);
    
    // load bots to the map
    void addBots(const rapidjson::Value& val);
    
    // adds bot to current level
    void addBot(BotPtr& bot);
    
    // adds a new spawn position for the current level
    void setSpawnPosition(const sf::Vector2f& newPosition);
    
    // adds a new spawn position for the current level
    void setSpawnPosition(const rapidjson::Value& obj);
    
    // returns the spawn position for the current level
    const sf::Vector2f& getSpawnPosition();
    
    // deletes the map
    void deleteMap(const unsigned int& level);
    
    // deletes the map
    void deleteMap(MapContainer::LEVEL level);
    
    // adds a new MapContainer to the class
    void insertMap(MapContainer::LEVEL level);
    
    // adds a new MapContainer to the class
    void insertMap(const size_t& level);
    
    // returns the current map/level
    MapContainer& getMap() const;
    
    // returns a specific map
    MapContainer& getMap(const size_t& level);
    
private:
    
    // contains every map in a pointer.
    std::map<MapContainer::LEVEL, Ptr>          m_mapHolder;
    
    // stores all the buildings
    std::vector<Building>                       m_buildings;
     
    // stores all the bots ( as unique pointer )
    std::vector<BotPtr>                         m_bots;
    
    // The floor of the level
    sf::RectangleShape                          m_floor;
    
    // Spawn position for the player
    sf::Vector2f                                m_spawnPosition;
    
    // Used to draw the current level or map the player is on.
    LEVEL                                       m_currentLevel;
    
    
};

#endif	/* MAPCONTAINER_HPP */

