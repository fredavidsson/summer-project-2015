
#include "ColRectangle.hpp"


ColRectangle::ColRectangle()
: shape()
, Collisionable()
{

}


ColRectangle::ColRectangle(sf::Vector2f size)
: shape(size)
, Collisionable(size)
{
    resetOrigin();
    shape.setFillColor(sf::Color(15, 15, 15));
}

void ColRectangle::setSize(const sf::Vector2f& size)
{
    shape.setSize(size);
    Collisionable::setBounds({0, 0, size.x, size.y});
    resetOrigin();
}

const sf::Color& ColRectangle::getFillColor()
{
    return shape.getFillColor();
}

const sf::Vector2f& ColRectangle::getSize()
{
    return shape.getSize();
}

void ColRectangle::resetOrigin()
{
    setOrigin(shape.getSize().x / 2.f, shape.getSize().y / 2.f);
}

void ColRectangle::changeColor(const sf::Color& color)
{
    if(color != shape.getFillColor())
        shape.setFillColor(color);
}

void ColRectangle::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(shape, states);
}
