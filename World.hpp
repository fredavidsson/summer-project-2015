#ifndef WORLD_HPP
#define	WORLD_HPP

#include <SFML/Graphics.hpp>

#include "Player.hpp"
#include "ColRectangle.hpp"
#include "CollisionHandler.hpp"
#include <iostream>
#include <vector>
#include "Map.hpp"

class World 
{
public:
    World(sf::RenderWindow& window);
    void render();
    void update(const sf::Time& deltaTime);
    void handleEvents(const sf::Event& event);
public:
    // Handles the collisions
    static CollisionHandler colHandler;
private:
    // handles the camera position, updates the position of the camera
    // to the player's position.
    void updateViewPosition();
private:
    sf::RenderWindow&   m_window;
    Player              m_player;
    sf::View            m_playersView;
    
    Map                 m_map;
    
    
};

#endif	/* WORLD_HPP */

