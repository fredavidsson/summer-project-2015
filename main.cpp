#include "Application.hpp"

#include <exception>

int main()
{
    try
    {
        Application app;
        app.run();
    } catch (std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}
