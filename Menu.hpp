/* 
 * File:   Menu.hpp
 * Author: Fredrik
 *
 */

#ifndef MENU_HPP
#define	MENU_HPP

#include <SFML/Graphics.hpp>
#include "Button.hpp"

class Menu
{
public:
    Menu(sf::RenderWindow& window);
    void toggle();
    void render();
    void update(const sf::Time& dt);
    void handleEvents(const sf::Event& event);
public:
    static bool MenuIsOpen;
    
private:
    
    // tells what button user is on
    enum Target
    {
        NewGame,
        Options,
        LoadGame,
        SaveGame,
        Exit
    };
    
    sf::RenderWindow&  _window;
    Button             _buttons;
    sf::RectangleShape _infoBackground;
    sf::RectangleShape _background;

    
};

#endif	/* MENU_HPP */

