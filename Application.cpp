
#include "Application.hpp"

Application::Application()
: _window(sf::VideoMode(1280, 720), "RPGGAME")
, _world(_window)
, _menu(_window)
{
    _window.setFramerateLimit(60);
}

void Application::run()
{
    sf::Clock clock;
    sf::Time  timeSinceLastUpdate = sf::Time::Zero;
    const sf::Time TimePerFrame(sf::seconds(1.f / 60.f));
    while(_window.isOpen())
    {
        timeSinceLastUpdate += clock.restart();
        while(timeSinceLastUpdate > TimePerFrame)
        {
            timeSinceLastUpdate -= TimePerFrame;
            handleEvents();
            update(TimePerFrame);
        }
        render();
    }
}

void Application::handleEvents()
{
    sf::Event event;
    while(_window.pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
        {
            _window.close();
        } else
        {
            _world.handleEvents(event);
            _menu.handleEvents(event);
        }
    }
}

void Application::update(const sf::Time& deltaTime)
{
    // updates the world
    _world.update(deltaTime);

    // Handles animations for the menu and updates it's position for the game.
    _menu.update(deltaTime);
}

void Application::render()
{
    _window.clear(sf::Color(2, 2, 2));
    _world.render();
    _menu.render();
    _window.display();
}
