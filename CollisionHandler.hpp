/* 
 * File:   CollisionHandler.hpp
 * Author: Pankie
 *
 */

#ifndef COLLISIONHANDLER_HPP
#define	COLLISIONHANDLER_HPP

#include "Collisionable.hpp"
#include "ColRectangle.hpp"

class CollisionHandler 
{
public:
    // prevents a rectangle shape, like buildings, from going through them.
    void handleObstacle(Collisionable& object_1, Collisionable& box2); 
    
    // checks if a rectangle shape is inside another rectangle
    bool collision(Collisionable& object, Collisionable& object_2);
    
private:

};

#endif	/* COLLISIONHANDLER_HPP */

