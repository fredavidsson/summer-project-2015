/* 
 * File:   Menu.cpp
 * Author: Fredrik
 * 
 */

#include "Menu.hpp"
#include "Application.hpp"
#include <iostream>


bool Menu::MenuIsOpen = false;

Menu::Menu(sf::RenderWindow& window)
: _window(window)
, _buttons(window)
, _infoBackground()
, _background(sf::Vector2f(_window.getSize()))
{
    _background.setFillColor(sf::Color(125, 180, 255, 50));
    _background.setOrigin(
        _background.getSize().x / 2.f, 
        _background.getSize().y / 2.f);
                
    _infoBackground.setSize({300.f, 284.f});
    _infoBackground.setFillColor(sf::Color(125, 180, 255));
    _infoBackground.setOrigin(
        _infoBackground.getSize().x / 2.f, _infoBackground.getSize().y / 2.f);
    
    // adding sample buttons
    for(unsigned int i = 0; i < 5; i++)
    {
        _buttons.addButton({0, 6 + (58.f * i)}, {142, 52});
    }
}

void Menu::update(const sf::Time& dt)
{
    if(MenuIsOpen)
    {
        _buttons.update(dt);
    }
}

void Menu::render()
{
    if(MenuIsOpen)
    {
        _window.draw(_background);
        _window.draw(_infoBackground);
        _window.draw(_buttons);
    }
}

void Menu::toggle()
{
    if(MenuIsOpen)
    {
        MenuIsOpen = false;
    }
    else
    {
        MenuIsOpen = true;
        
        // get the position of the view and move the menu to the screen.
        const sf::Vector2f viewPosition = _window.getView().getCenter();
        _background.setPosition(viewPosition);
        _buttons.setPosition(viewPosition.x - 160.f, viewPosition.y - 122.f);
        
        _infoBackground.setPosition(viewPosition.x + 70.f, viewPosition.y);
    }

}

void Menu::handleEvents(const sf::Event& event)
{
    
    switch(event.type)
    {
    case event.KeyPressed:
        // toggle menu on escape
        if(event.key.code == sf::Keyboard::Escape)
        {
            toggle();
        }
        if(MenuIsOpen)
        {
            // change selected button
            if(event.key.code == sf::Keyboard::W)
            {
                _buttons.selectPreviousButton();
            }
            if(event.key.code == sf::Keyboard::S)
            {
                _buttons.selectNextButton();
            }
            if(event.key.code == sf::Keyboard::Space)
            {
                _buttons.selectButton();
            }
        }
        
        break;
    default:
        break;
    }
}
